_**Ceci est un template de sujet, à dupliquer pour proposer un readme pour présenter un vrai sujet aux participant·es.**_

---


# # Sujet n°X : Titre du sujet

+ Thème : Lorem ipsum.
+ Compétences associés : lorem ipsum.


##### #1 | Présentation de la structure portant le projet
Lorem ipsum.


##### #2 | Problématique
Lorem ipsum.


##### #3 | Le défi proposé
Lorem ipsum.


##### #4 | Livrables
Lorem ipsum.


##### #5 | Ressources à disposition pour résoudre le défi
Lorem ipsum.


##### #6 | Code de conduite et philosophie du hackathon
Lors de la conception du hackathon, Latitudes a voulu prendre des partis-pris afin de rendre celui-ci particulier. 

Il s’agit d’éléments que nous souhaitons incarner collectivement avec vous tout au long de la journée :
+ La force du collectif pour faire émerger des solutions adaptées aux porteur·ses de projets, notamment via la session de stand-up meeting ;
+ Une attention portée à la conception rapide, au test et à l’itération qui doivent permettre aux porteur·ses de projets d’avoir accès aux solutions les plus abouties possibles ;
+ L’engagement sur des solutions ouvertes, facilement utilisables par les porteur·ses de projets, et qui vous permettront de continuer à contribuer à l’issue du hackathon si vous le souhaitez, ou de permettre à d’autres personnes de le faire.

##### #7 | Points de contact lors du hackathon
+ Lorem ipsum.